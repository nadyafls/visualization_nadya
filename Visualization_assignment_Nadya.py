#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')


# In[2]:


x = np.arange(0,100)
y = x*2
z = x**2


# In[3]:


fig = plt.figure()

ax = fig.add_axes([0,0,1,1])

ax.plot(x,y)
ax.set_title('title')
ax.set_xlabel('x')
ax.set_ylabel('y')


# In[4]:


print ("Hi")


# In[5]:


fig = plt.figure()

ax1 = fig.add_axes([0,0,1,1])
ax2 = fig.add_axes([.2,.5,.2,.2])


# In[6]:


fig = plt.figure()

ax1 = fig.add_axes([0,0,1,1])
ax2 = fig.add_axes([.2,.5,.2,.2])

ax1.plot(x,y,'r')
ax1.set_xlabel('x')
ax1.set_ylabel('y')

ax2.plot(x,y,'r')
ax2.set_xlabel('x')
ax2.set_ylabel('y')


# In[7]:


fig = plt.figure()

ax1 = fig.add_axes([0,0,1,1])
ax2 = fig.add_axes([.2,.5,.4,.4])


# In[8]:


ax1.plot(x,z,color='blue')
ax1.set_xlabel('X')
ax1.set_ylabel('Z')

ax2.plot(x,y,color='blue')
ax2.set_title('zoom')
ax2.set_xlabel('X')
ax2.set_ylabel('Y')
ax2.set_xlim((20,22))
ax2.set_ylim((30,50))

fig


# In[9]:


fig, axes = plt.subplots(nrows=1,ncols=2)


# In[10]:


axes[0].plot(x,y,color='blue',lw=3,ls='--')
axes[1].plot(x,z,color='red',lw=3)

fig


# In[11]:


fig, axes= plt.subplots(nrows=1,ncols=2,figsize=(12,2))
axes[0].plot(x,y,color='blue',lw=3,ls='--')
axes[1].plot(x,z,color='red',lw=3)

fig;


# In[12]:


df3 = pd.read_csv('df3')


# In[13]:


df3.info()


# In[14]:


df3.head()


# In[15]:


df3.plot.scatter(x='a',y='b',c='r',figsize=(10,3))


# In[16]:


plt.margins(0,.05)
df3['a'].hist()


# In[17]:


plt.style.use('ggplot')
plt.margins(0,.05)
df3['a'].hist(bins=25,alpha=.5)


# In[18]:


df3.boxplot(column=['a','b'])


# In[19]:


plt.margins(0,0.05)
df3['d'].plot.kde()


# In[20]:


plt.margins(0,0.05)
df3['d'].plot.kde(ls='dashed',lw=3)


# In[21]:


df3.head(30).plot.area(alpha=.4)


# In[ ]:





# In[ ]:




