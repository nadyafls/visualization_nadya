#!/usr/bin/env python
# coding: utf-8

# In[1]:


import matplotlib.pyplot as plt
import numpy as np


# In[2]:


get_ipython().run_line_magic('matplotlib', 'inline')


# In[3]:


plt.show()


# In[4]:


x = np.linspace (0, 5, 11)
y = x ** 2


# In[5]:


x


# In[6]:


y


# In[7]:


plt.plot (x, y, 'r') # 'r' buat grafik warna merah
plt.xlabel ('X Axis Title Here')
plt.ylabel ('Y Axis Title Here')
plt.title ('String Title Here')
plt.show()


# In[8]:


#plt.subplot (nrows, ncols, plot_number)
plt.subplot (1,2,1)
plt.plot (x, y, 'r--') # 'r--' buat grafik nya jadi garis putus putus
plt.subplot (1,2,2)
plt.plot (y, x, 'g*-'); # 'g' buat grafik hijau dan '*' buat penandaan tituk koordinat


# In[9]:


# Creat Figure (empety canvas)
fig = plt.figure()

# Add set of axes to figure
axes = fig.add_axes ([0.1, 0.1, 0.8, 0.8]) # left, bottom, width, height (range 0 to 1)

# Plot on that set of axes
axes.plot(x, y, 'b')
axes.set_xlabel ('Set X Label') # Notice the use of set_ to begin methods
axes.set_ylabel ('Set Y Label')
axes.set_title ('Set Title')


# In[10]:


# Create blank canvas
fig = plt.figure()

axes1 = fig.add_axes ([0.1, 0.1, 0.8, 0.8]) # main axes
axes2 = fig.add_axes ([0.2, 0.5, 0.4, 0.3]) #inset axes

# Larger Figure Axes 1
axes1.plot(x, y, 'b')
axes1.set_xlabel ('X_label_axes2')
axes1.set_ylabel ('Y_label_axes2')
axes1.set_title ('Axes 2 Title')

# Larger Figure Axes 2
axes2.plot(y, x, 'r')
axes2.set_xlabel ('X_label_axes2')
axes2.set_ylabel ('Y_label_axes2')
axes2.set_title ('Axes 2 Title');


# In[11]:


# Use similar to plt.figure() except use tuple unpacking to grab fig and axes
fig, axes = plt.subplots()

# Now use the axes object to add stuff to plot
axes.plot(x, y, 'r')
axes.set_xlabel ('X')
axes.set_ylabel ('Y')
axes.set_title ('title');


# In[12]:


# Empety canvas of 1 by 2 subplots
fig, axes = plt.subplots(nrows = 1, ncols = 2)


# In[13]:


# Axes is an array of axes to plot on
axes


# In[14]:


# we can iterate through this array
for ax in axes:
    ax.plot(x, y, 'b')
    ax.set_xlabel ('x')
    ax.set_ylabel ('y')
    ax.set_title ('title')

# Disply the figure object
fig


# In[15]:


fig, axes = plt.subplots(nrows=1, ncols=2)

for ax in axes:
    ax.plot(x, y, 'g')
    ax.set_xlabel ('x')
    ax.set_ylabel ('y')
    ax.set_title ('title')

fig
plt.tight_layout()


# In[16]:


fig = plt.figure(figsize=(8,4), dpi=100)


# In[17]:


fig, axes = plt.subplots (figsize = (12,3))
axes.plot(x, y, 'r')
axes.set_xlabel ('x')
axes.set_ylabel ('y')
axes.set_title ('title');


# In[18]:


fig.savefig("filename.png")


# In[19]:


fig.savefig ("filename.png", dpi = 200)


# In[20]:


ax.set_title ('title');


# In[21]:


ax.set_xlabel('x')
ax.set_ylabel('y');


# In[22]:


fig = plt.figure()

ax = fig.add_axes([0,0,1,1])

ax.plot (x, x**2, label='x**2')
ax.plot (x, x**3, label='x**3')
ax.legend()


# In[23]:


#lots of options
ax.legend(loc=1) #upper right corner
ax.legend(loc=2) #upper left corner
ax.legend(loc=3) #lower left corner
ax.legend(loc=4) #lower right corner

# .. many more options are avaible
#most common to choose
ax.legend (loc=0) # let matplotlib decide the optimal location
fig


# In[24]:


# MATLAB style line color and style
fig, ax = plt.subplots()
ax.plot(x, x**2, 'b--') #blue line with dots
ax.plot(x, x**3, 'g--') #green dashed line


# In[25]:


fig, ax = plt.subplots()

ax.plot(x, x+1, color = 'blue', alpha = 0.5) # half-transparant
ax.plot(x, x+2, color = '#8B008B') # RGB hex code
ax.plot(x, x+3, color = '#FF8C00') # RGB hex code


# In[28]:


fig, ax = plt.subplots (figsize = (12,6))
ax.plot(x, x+1, color = "red", linewidth = 0.25)
ax.plot(x, x+2, color = "red", linewidth = 0.50)
ax.plot(x, x+3, color = "red", linewidth = 1.00)
ax.plot(x, x+4, color = "red", linewidth = 2.00)

#possible linestyle options '-', '-', '-.', ':', 'steps'
ax.plot(x, x+5, color = 'green', lw=3, linestyle='-')
ax.plot(x, x+6, color = 'green', lw=3, ls='-.')
ax.plot(x, x+7, color = 'green', lw=3, ls=':')

#custom dash
line, = ax.plot(x, x+8, color = 'black', lw=1.50)
line.set_dashes([5, 10, 15, 10]) # format: line length, space length

# possible marker symbols: maker ='+', 'o', '*', 's', ',', '.', '1', '2', '3', '4'
ax.plot(x, x+9, color = 'blue', lw=3, ls='-', marker='+')
ax.plot(x, x+10, color = 'blue', lw=3, ls='--', marker='o')
ax.plot(x, x+11, color = 'blue', lw=3, ls='-', marker='s')
ax.plot(x, x+12, color = 'blue', lw=3, ls='-', marker='1')

# marker size and color
ax.plot(x, x+13, color = 'purple', lw=1, ls='-', marker='o', markersize=2)
ax.plot(x, x+14, color = 'purple', lw=1, ls='-', marker='o', markersize=4)
ax.plot(x, x+15, color = 'purple', lw=1, ls='-', marker='o', markersize=8, markerfacecolor = 'red')
ax.plot(x, x+16, color = 'purple', lw=1, ls='-', marker='s', markersize=8,
       markerfacecolor = 'yellow', markeredgewidth = 3, markeredgecolor = 'green');


# In[30]:


fig, axes = plt.subplots (1, 3, figsize = (12,4))
axes[0].plot(x, x**2, x, x**3)
axes[0].set_title('default axes ranges')

axes[1].plot(x, x**2, x, x**3)
axes[1].axis ('tight')
axes[1].set_title ('tight axes')

axes[2].plot(x, x**2, x, x**3)
axes[2].set_ylim ([0,60])
axes[2].set_xlim ([2, 5])
axes[2].set_title ('costum axes range');


# In[31]:


plt.scatter(x,y)


# In[33]:


from random import sample
data = sample (range(1, 1000), 100)
plt.hist(data)


# In[36]:


data = [np.random.normal(0, std, 100) for std in range (1, 4)]

# rectangular box plot
plt.boxplot(data, vert = True, patch_artist = True);


# In[37]:


import numpy as np
import pandas as pd
get_ipython().run_line_magic('matplotlib', 'inline')


# In[39]:


df1 = pd.read_csv('df1',index_col = 0)
df2 = pd.read_csv('df2')


# In[40]:


df1['A'].hist()


# In[41]:


import matplotlib.pyplot as plt
plt.style.use('ggplot')


# In[42]:


df1['A'].hist()


# In[43]:


plt.style.use('bmh')
df1['A'].hist()


# In[45]:


plt.style.use('dark_background')
df1['A'].hist()


# In[46]:


plt.style.use('fivethirtyeight')
df1['A'].hist()


# In[48]:


df1['A'].hist()


# In[49]:


plt.style.use('ggplot')


# In[50]:


df2.head()


# In[51]:


df2.plot.bar()


# In[52]:


df2.plot.bar(stacked = True)


# In[54]:


df2.plot.area (alpha = 0.4)


# In[58]:


df1 ['A'].plot.hist(bins = 50)


# In[1]:


df1.plot.line(x =df1.index,y='B',figsize =(12,3), lw=1)


# In[60]:


df1.plot.scatter(x ='A', y ='B')


# In[62]:


df1.plot.scatter (x='A',y='B',c='C', cmap = 'coolwarm')


# In[63]:


df1.plot.scatter(x='A',y='B',s=df1['C']*200)


# In[64]:


df2.plot.box() # can also pass a by = argument for groupby


# In[70]:


df = pd.DataFrame (np.random.randn (100, 2), columns=['a','b'])
df.plot.hexbin(x='a',y='b', gridsize=25,cmap='Oranges')


# In[2]:


df2['a'].plot.kde()


# In[75]:


df2.plot.density()


# In[7]:


import pandas as pd
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')


# In[8]:


mcdon = pd.read_csv ('mcdonalds.csv', index_col = 'Date', parse_dates = True)


# In[9]:


mcdon.head()


# In[10]:


#not good!
mcdon.plot()


# In[11]:


mcdon['Adj. Close'].plot()


# In[12]:


mcdon ['Adj. Volume'].plot()


# In[13]:


mcdon['Adj. Close']. plot(figsize=(12,8))


# In[14]:


mcdon['Adj. Close'].plot(figsize=(12,8))
plt.ylabel('Close Price')
plt.xlabel('Ocerwrite Date Index')
plt.title('mcdonalds')


# In[15]:


mcdon['Adj. Close'].plot(figsize=(12,8), title='Pandas Title')


# In[16]:


# Plot formating
# X limits
mcdon['Adj. Close']. plot(xlim=['2007-01-01', '2009-01-01'])


# In[17]:


mcdon['Adj. Close'].plot(xlim=['2007-01-01','2009-01-01'],ylim=[0,50])


# In[18]:


mcdon['Adj. Close'].plot(xlim=['2007-01-01','2007-05-01'],ylim=[0, 40],ls='--',c='r')


# In[23]:


# X ticks
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as dates


# In[24]:


mcdon ['Adj. Close'].plot(xlim=['2007-01-01','2007-05-01'],ylim=[0,40])


# In[30]:


idx = mcdon.loc['2007-01-01':'2007-05-01'].index
stock = mcdon.loc['2007-01-01':'2007-05-01']['Adj. Close']


# In[31]:


idx


# In[32]:


stock


# In[33]:


#basic matplotlib plot
fig, ax = plt.subplots()
ax.plot_date(idx, stock,'-')
plt.tight_layout()
plt.show()


# In[35]:


# fix the overlap
fig, ax = plt.subplots()
ax.plot_date(idx, stock,'-')

fig.autofmt_xdate() #Auto fixes the overlap!
plt.tight_layout()
plt.show()


# In[36]:


# costomize grid
fig, ax = plt.subplots()
ax.plot_date(idx, stock,'-')
ax.yaxis.grid(True)
ax.xaxis.grid(True)
fig.autofmt_xdate() #aut fixes the overlap
plt.tight_layout()
plt.show()


# In[37]:


fig, ax = plt.subplots()
ax.plot_date(idx, stock,'-')

#grids
ax.yaxis.grid(True)
ax.xaxis.grid(True)

#Major Axis
ax.xaxis.set_major_locator(dates.MonthLocator())
ax.xaxis.set_major_formatter(dates.DateFormatter('\n\n\n\n%Y--%B'))

fig.autofmt_xdate() # Auto fixes the Overlap
plt.tight_layout()
plt.show()


# In[38]:


# minor axis
fig, ax = plt.subplots()
ax.plot_date(idx, stock,'-')

# Major Axis
ax.xaxis.set_major_locator(dates.MonthLocator())
ax.xaxis.set_major_formatter(dates.DateFormatter('\n\n%Y--%B'))

#Minor Axis
ax.xaxis.set_minor_locator(dates.WeekdayLocator())
ax.xaxis.set_minor_formatter(dates.DateFormatter('%d'))

#Grids
ax.yaxis.grid(True)
ax.xaxis.grid(True)

fig.autofmt_xdate() # Auto fixes the Overlap
plt.tight_layout()
plt.show()


# In[43]:


fig, ax = plt.subplots(figsize = (10,8))
ax.plot_date(idx, stock,'-')

# Major Axis
ax.xaxis.set_major_locator(dates.WeekdayLocator(byweekday=1))
ax.xaxis.set_major_formatter(dates.DateFormatter('%B-%d-%a'))

#grids
ax.yaxis.grid(True)
ax.xaxis.grid(True)

fig.autofmt_xdate() # Auto fixes the overlap

plt.tight_layout()
plt.show()
                                    


# In[ ]:




